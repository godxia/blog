# 运行100万个并发任务，不同语言各需要多少内存

作者：DataStax 公司（美国的一家数据库系统开发商）Piotr Kołaczkowski

原文见：
https://pkolaczk.github.io/memory-consumption-of-async/


在这篇博客文章中，探讨了处理大量网络连接时候的Rust、Go、Java、C#、Python、Node.js和Elixir等流行语言中的内存消耗比较。

在不久之前，遇上了一个必须比较几个计算机程序设计来处理大量网络连接的并发需求，看到各种程序在处理大量网络连接时的内存消耗差异很大，甚至超过20倍！

其中有些程序消耗很少超过100MB，但是有些程序（在1万并发）的连接中消耗达到了3GB，不过，由于这些程序非常复杂，而且各种框架的特性也不同，所以很难直接比较它们并得出有意义的结论，因为这不是一种苹果对另外一种苹果的直接比较。

因此，我产生了一种合成基准的想法，即创建一个合成基准来比较各种编程语言的内存消耗。

## 基准测试

在下面，我们使用不同的编程语言编写了以下程序：

> 启动N个并发任务，每个任务等待10秒，然后所有任务完成后程序退出。任务数量由命令行参数控制。

因为我们现在可以借助ChatGPT，所以可以轻松地使用那些平时很少使用的编程语言，几分钟就能写出来这种程序。为了让大家使用方便，已经在github上全部发布，测试代码也一并附上。

地址：
https://github.com/pkolaczk/async-runtimes-benchmarks

## Rust

在Rust里面，编写了三种解决方案，第一种采用了传统的多线程模式：

```
let mut handles = Vec::new();
for _ in 0..num_threads {
    let handle = thread::spawn(|| {
        thread::sleep(Duration::from_secs(10));
    });
    handles.push(handle);
}
for handle in handles {
    handle.join().unwrap();
}
```

后面两种方式都采用的是异步模式，分别用了tokio框架和async-std框架：

下面是tokio框架的异步代码：

```
let mut tasks = Vec::new();
for _ in 0..num_tasks {
    tasks.push(task::spawn(async {
        time::sleep(Duration::from_secs(10)).await;
    }));
}
for task in tasks {
    task.await.unwrap();
}
```

最后因为async-std方案与tokio方法的实现非常相似，这里就不直接贴出来了。

## Go

在 Go 中，使用了并发常用的构建块：goroutines ，当然我们不会单独使用它们，而是使用WaitGroup：

```
var wg sync.WaitGroup
for i := 0; i < numRoutines; i++ {
    wg.Add(1)
    go func() {
        defer wg.Done()
        time.Sleep(10 * time.Second)
    }()
}
wg.Wait()
```

## Java

Java处理此类问题在传统上会使用线程，但JDK 21提供了虚拟线程，这是与goroutines的概念类似。因此，我们创建了基准测试的两个变体。因为我也很好奇Java线程与Rust的线程相比效果到底如何。

```
List<Thread> threads = new ArrayList<>();
for (int i = 0; i < numTasks; i++) {
    Thread thread = new Thread(() -> {
        try {
            Thread.sleep(Duration.ofSeconds(10));
        } catch (InterruptedException e) {
        }
    });
    thread.start();
    threads.add(thread);
}
for (Thread thread : threads) {
    thread.join();
}
```

这是带有虚拟线程的变体。请注意它是多么相似！几乎一模一样！

```
List<Thread> threads = new ArrayList<>();
for (int i = 0; i < numTasks; i++) {
    Thread thread = Thread.startVirtualThread(() -> {
        try {
            Thread.sleep(Duration.ofSeconds(10));
        } catch (InterruptedException e) {
        }
    });
    threads.add(thread);
}
for (Thread thread : threads) {
    thread.join();
}
```

## C#

C#，类似于Rust，对async/await有一流的支持：

```
List<Task> tasks = new List<Task>();
for (int i = 0; i < numTasks; i++)
{
    Task task = Task.Run(async () =>
    {
        await Task.Delay(TimeSpan.FromSeconds(10));
    });
    tasks.Add(task);
}
await Task.WhenAll(tasks);
```

## Node.JS

```
const delay = util.promisify(setTimeout);
const tasks = [];

for (let i = 0; i < numTasks; i++) {
    tasks.push(delay(10000);
}

await Promise.all(tasks);
```

## Python

Python使用async/await特性

```
async def perform_task():
    await asyncio.sleep(10)


tasks = []

for task_id in range(num_tasks):
    task = asyncio.create_task(perform_task())
    tasks.append(task)

await asyncio.gather(*tasks)
```

## Elixir

Elixir也以其异步功能而闻名：

```
tasks =
    for _ <- 1..num_tasks do
        Task.async(fn ->
            :timer.sleep(10000)
        end)
    end

Task.await_many(tasks, :infinity)
```

## 测试环境

* Hardware: Intel(R) Xeon(R) CPU E3-1505M v6 @ 3.00GHz
* OS: Ubuntu 22.04 LTS, Linux p5520 5.15.0-72-generic
* Rust: 1.69
* Go: 1.18.1
* Java: OpenJDK “21-ea” build 21-ea+22-1890
* .NET: 6.0.116
* Node.JS: v12.22.9
* Python: 3.10.6
* Elixir: Erlang/OTP 24 erts-12.2.1, Elixir 1.12.2

所有程序都使用release 模式（如果可用）启动。其他选项保留为默认值。

## 测试结果

### 最小占用空间

让我们从最少开销做起。由于程序的某些运行时本身就需要一些内存，因此让我们首先只启动一个任务。

![img](./image/1.png)

图例.1：启动一个任务所需的峰值内存

在最小需求的情况下，我们很明确的可以看见有两类程序：

* 以Rust 和Go这样的静态编译为本机二进制代码的程序，在启动的时候，仅需要很少的内存；

* 而以通过解释器运行的程序，在启动的时候需要消耗更多内存……虽然Python在这种情况下表现出了很好的效果，但是对比静态编译的程序，这两类程序语言之间的内存消耗还是有明确的差距，大约是一个数量级。

令我们感到惊讶的是，.NET 不知何故具有最糟糕的效果，但这些东西应该可以通过一些设置进行调整。因为我们并不太熟悉.NET的开发和调优，如果有相关任何技巧，请在评论中告诉我。整体而言，并还没有看到debug模式和release模式之间有太大区别。

## 1万并发任务

![img](./image/2.png)

图例.2：启动1万个并发任务所需的峰值内存

这里出现了一些惊喜！之前有过开发经验的人，都可能预料到使用线程会成为这个基准测试的大输家。特别对于 Java 线程来说也是如此，它确实消耗了近 250 MB 的 RAM。

但是同样使用线程来处理，Rust则表现好得多：Rust使用的是原生Linux线程，所以似乎足够轻量级，在10k线程下，内存消耗仍然低于许多其他运行时的空闲内存消耗。Java的异步任务或虚拟线程效果也似乎比本机线程轻，但我们不会在仅1万个并发这种简单任务中看到这种优势。我们需要给它们更多的压力。

这里的另一个惊喜是Go。按道理来说，Goroutines 应该是非常轻量级的，但它们实际上消耗了比Rust线程更多达50% 以上的 RAM。老实说，我期待的是在更大的差异下，出现有利于Go的结论。

因此，这里可以得出第一个结论，起码在1万个并发任务中，线程仍然是一个相当有竞争力的选择。我们猜想，Linux内核肯定在这里做了一些事情。

Go 在这个的基准测试中也失去了它对 Rust 异步的微小优势，而现在它消耗的内存比最好的 Rust 程序多 6 倍以上。甚至它都被Python超越了。

最后一个惊喜是，在1万并发这个任务中，.NET 的内存消耗并没有因空闲内存使用而显着增加。我们猜想，在启动时候消耗的内存，可能只是它在使用预分配的内存。但是有个可能就是它的空闲内存使用率如此之高，以至于一万并发对他来说，压力太少而感觉无关紧要。

## 10万并发

在我的系统上，无法启动 10万个线程，因此必须排除使用线程来进行基准测试。可能这可以通过更改系统设置以某种方式进行调整，但是在尝试了一个小时后，我放弃了。因此，在10万并发中，打架可能不想使用线程。

![img](./image/3.png)

在10万并发的情况下，Go程序不仅被Rust击败，还被接连被Java，C#和Node.JS击败，已经降到了第四位……

我一度怀疑在Linux上，.NET的程序可能出现了作弊情况，因为它的内存使用量仍然没有上升。我不得不仔细检查它是否真的启动了正确数量的任务，但确实如此。大约 10 秒后它仍然正确结束，因此不会阻塞主循环。简直就是黑魔法！干得漂亮！.NET。

## 100万并发

现在进入测试的高潮，我们用100万个并发来进行极端测试。

在100万个任务中，Elixir直接崩溃了，提示：
* ** (SystemLimitError) a system limit has been reached

> ps:一些评论者指出我可以增加进程限制。在将 --erl '+P 1000000' 参数添加到Elixir调用后，它运行良好。

![img](./image/4.png)

最后，我们看到 C# 程序的内存消耗虽然有所增加。但它仍然是今天冠军的有力竞争者。它甚至设法略微击败了 Rust 的异步框架之一！

让我感到意外的是，Go 和其他语言之间的距离增加了。现在Go输给赢家（Rust tokio框架）的开销超过12倍。同时它甚至输给了Java的2倍以上，这与JVM是内存消耗和Go是轻量级的普遍看法相矛盾。

最终的冠军是Rust tokio框架，它在所有的测试项中，表现仍然无与伦比。特别是在最后的100万并发测试中。

## 结语

正如我们所观察到的，大量并发任务会消耗大量内存，即使它们不执行复杂的操作。不同的语言运行时有不同的权衡，有些对于少量任务来说是轻量级和高效的，但对于数十万个任务来说，扩展性就表现得很差。

相反，.NET、Java这种具有高初始开销的其他运行时可以毫不费力地处理高工作负载。需要注意的是，并非所有运行时都能够使用默认设置处理大量并发任务。

当然，今天的测试，只关注内存的消耗，而在真正的高并发应用中，任务启动执行时间和通信速度等其他因素同样重要。值得注意的是，在 100 万个并发的任务中，启动任务的开销变得明显，大多数程序需要超过 12 秒才能完成。

有兴趣的同学，请继续关注即将到来的基准测试，我们未来将深入探讨其他方面。