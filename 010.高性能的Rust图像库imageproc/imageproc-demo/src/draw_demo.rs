use image::{Rgb, Rgba,RgbImage,RgbaImage};
use imageproc::drawing::Canvas;
use imageproc::drawing::{
    draw_cross_mut, draw_filled_circle_mut,  draw_filled_rect_mut,
        draw_hollow_rect_mut, draw_filled_ellipse_mut,draw_text_mut,draw_polygon_mut,
        draw_antialiased_line_segment_mut,draw_line_segment_mut
};

use imageproc::rect::Rect;
use std::collections::{HashSet,HashMap};
use imageproc::pixelops::{interpolate};
use ab_glyph::{FontRef, PxScale};
#[cfg(test)]
mod tests{
    
    use super::*;

    const red:Rgb<u8>   = Rgb([255u8, 0u8,   0u8]);
    const green:Rgb<u8> = Rgb([0u8,   255u8, 0u8]);
    const blue :Rgb<u8> = Rgb([0u8,   0u8,   255u8]);
    const white:Rgb<u8> = Rgb([255u8, 255u8, 255u8]);

    #[test]
    fn test_draw_rgba(){
        //设置背景透明
        let mut image = RgbaImage::new(800,600);
        for (x, y, pixel) in image.enumerate_pixels_mut() {
            *pixel = image::Rgba([0, 0, 0, 0]);
        }
        let r2 = Rect::at(15,15).of_size(200, 200);
        //rgba ，a表示透明度，0-255，0表示全部透明，255表示完全不透明
        draw_filled_rect_mut(&mut image,r2,Rgba([0u8,255u8,0u8,120u8]));
        image.save("abc.png").unwrap();
    }
    #[test]
    fn test_draw_1(){
        let mut image = RgbImage::new(800, 600);
        //image填充背景色（灰度值），0黑色-255白色，
        image.fill(255u8);

        //标准像素就是一个宽高为1的rect
        let r1 = Rect::at(5,5).of_size(1, 1);
        let r2 = Rect::at(15,15).of_size(2, 2);
        let r3 = Rect::at(25,25).of_size(3, 3);
        let r4 = Rect::at(35,35).of_size(4, 4);

        //填充矩形
        draw_filled_rect_mut(&mut image,r1,green);
        draw_filled_rect_mut(&mut image,r2,green);
        draw_filled_rect_mut(&mut image,r3,green);
        draw_filled_rect_mut(&mut image,r4,green);

        //绘制空心矩形，注意，如果要绘制带边框的，需要先填充再包边，后面的会覆盖前面的
        //1个像素的，就没有空心这一说了
        draw_hollow_rect_mut(&mut image,r1,red);
        draw_hollow_rect_mut(&mut image,r2,red);
        draw_hollow_rect_mut(&mut image,r3,red);
        draw_hollow_rect_mut(&mut image,r4,red);

        //绘制十字，一个十字是以中心坐标为中心点的十字结构，五个像素
        draw_cross_mut(&mut image,red, 50,100);
        draw_cross_mut(&mut image,red, 50,110);
        draw_cross_mut(&mut image,red, 50,120);
        draw_cross_mut(&mut image,red, 50,130);

        //绘制填充圆
        //1个像素的圆等于一个cross，半径单位也是像素
        draw_filled_circle_mut(&mut image,(100,50),1,red);
        draw_filled_circle_mut(&mut image,(100,55),2,red);
        draw_filled_circle_mut(&mut image,(100,70),5,red);
        draw_filled_circle_mut(&mut image,(100,100),10,red);
        draw_filled_circle_mut(&mut image,(100,150),15,red);

        //绘制椭圆形
        draw_filled_ellipse_mut(&mut image,(200,50),5,8,red);
        draw_filled_ellipse_mut(&mut image,(200,60),5,15,red);
        draw_filled_ellipse_mut(&mut image,(200,80),5,25,red);
        draw_filled_ellipse_mut(&mut image,(200,150),15,10,red);
        draw_filled_ellipse_mut(&mut image,(200,180),40,15,red);

        image.save("abc.png").unwrap();
    }

    use std::char::from_u32;
    #[test]
    fn test_draw_font(){
        // let font_data = include_bytes!("../font/msyh.ttf");
        // let font = Vec::from(font_data as &[u8]);
        //let font_data = std::fs::read("./font/msyh.ttf").unwrap();

        let font = FontRef::try_from_slice(include_bytes!("../font/msyh.ttf")).unwrap();

        //字体的比例尺，也就是占多少个像素
        let height =50.0;
        let scale = PxScale {
            x: height,
            y: height,
        };
        
        
        let mut image = RgbImage::new(800, 600);
        image.fill(255u8);
        let text = "你好世界！";
        draw_text_mut(&mut image, red, 10, 10, scale, &font, text);
        

        let font = FontRef::try_from_slice(include_bytes!("../font/esri_1.ttf")).unwrap();
        //字体图形，用u32的unicode就行
        let mut s = String::new();
        for i in 0..20{
            s.push(from_u32(33+i).unwrap());
        }
        
        draw_text_mut(&mut image, red, 10, 60, scale, &font, s.as_str());
        image.save("outimage/abc2.png").unwrap();

    }
    
    #[test]
    fn test_draw_line(){
        let mut image = RgbImage::new(50, 50);
        image.fill(255u8);

        
        draw_antialiased_line_segment_mut(&mut image,(10,10), (10,10), blue,interpolate);
        // draw_antialiased_line_segment_mut(&mut image,(0,0), (100,100), blue,interpolate);
        // draw_antialiased_line_segment_mut(&mut image,(0,1), (100,101), blue,interpolate);

        // draw_antialiased_line_segment_mut(&mut image,(0,99), (100,149), red,interpolate);
        // draw_antialiased_line_segment_mut(&mut image,(0,100), (100,150), red,interpolate);
        // draw_antialiased_line_segment_mut(&mut image,(0,101), (100,151), red,interpolate);

        

        //draw_filled_rect_mut(&mut image, Rect::at(100, 100).of_size(150, 100), red);
       
        image.save("line.png").unwrap();
    }

  
    use image::Pixel;
    use imageproc::map::map_colors2;
    use std::{i64, time::SystemTime};
    #[test]
    fn test_image2_map(){
        let mut image1 = RgbImage::new(3000, 3000);
        let r1 = Rect::at(5,5).of_size(100, 100);

        let mut image2 = RgbImage::new(3000, 3000);
        let r2 = Rect::at(15,15).of_size(120, 120);
        let time_a = SystemTime::now();
        draw_filled_rect_mut(&mut image1, r1, red);
        draw_filled_rect_mut(&mut image2, r2,blue);

        
    
        //map_colors2可以迭代对比两个image，然后可以处理两个image的像素关系
        //自己写for循环也行……
        let image_all = map_colors2(&image1, &image2, |i1,i2|{
            let rgb:[u8;3] = i1.0;
            let rgb2:[u8;3] = i2.0;
            if rgb2[0] ==0 && rgb2[1] ==0 && rgb2[2] ==0{
                i1
            }else{
                i2
            }
        });

        let time_b = SystemTime::now().duration_since(time_a);
        println!("{:?}", time_b);
        image_all.save(format!("megre{}.png",2)).unwrap();

    }
}