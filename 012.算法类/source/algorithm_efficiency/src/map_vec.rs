use std::collections::HashMap;

use chrono::prelude::*;
use chrono::*;
use rand::rngs::ThreadRng;
use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;

//生成指定长度的随机字符串
fn generate_random_string(mut rng: ThreadRng, length: usize) -> String {
    
    std::iter::repeat(())
        .map(|_| rng.sample(Alphanumeric))
        .map(|c| c as char) 
        .take(length)
        .collect::<String>()
}

pub fn map_vec_eff1(){
    let size = 1000000;
    let mut rng = rand::thread_rng();
    let mut vec_info:Vec<(String,i32)> = Vec::with_capacity(size);
    let mut hashmap_info:HashMap<usize,(String,i32)> = HashMap::with_capacity(size);

    for i in 0..size{
        //生成一个abc.abcde这样格式的模拟名字
        let name = format!("{}.{}",generate_random_string(rng.to_owned(),3),generate_random_string(rng.to_owned(),5));
        //生成一个随机年龄
        let age = rng.gen_range(22..=60);
        vec_info.push((name.to_owned(), age));
        hashmap_info.insert(i, (name.to_owned(), age));
    }
    
    // let start = Utc::now();
    // for i in 0..1000{
    //     let idx = rng.gen_range(0..size);
    //     println!("name= {} age = {}",vec_info[idx].0,vec_info[idx].1);
    // }
    // let end = Utc::now();
    // let vec_o = end.timestamp_subsec_micros()-start.timestamp_subsec_micros();
    

    // let start = Utc::now();
    // for i in 0..1000{
    //     let idx = rng.gen_range(0..size);
    //     println!("{:?}",hashmap_info[&idx]);
    // }
    // let end = Utc::now();

    // let hm_o = end.timestamp_subsec_micros()-start.timestamp_subsec_micros();
    // println!("Vec find 耗时{} 微秒",vec_o);
    // println!("hashmap find 耗时{} 微秒",hm_o);

    let start = Utc::now();
    for info in vec_info{
        let x = info.to_owned();
    }
    let end = Utc::now();
    let vec_iter = end.timestamp_subsec_micros()-start.timestamp_subsec_micros();

    let start = Utc::now();
    for info in hashmap_info{
        let x = info.to_owned();
    }
    let end = Utc::now();
    let hm_iter = end.timestamp_subsec_micros()-start.timestamp_subsec_micros();

    println!("Vec iter 耗时{} 微秒",vec_iter);
    println!("hashmap iter 耗时{} 微秒",hm_iter);

}

mod tests{
    use super::*;
    #[test]
    fn test_map_vec_eff1(){
        map_vec_eff1()
    }
}