import pandas
import shapely

def two_multi_float_py(x:float, y:float)->float:
    return x*y

def two_divi_float_py(x:float, y:float) -> float:
    return x/y

def int_sum_py(x:int)->int:
    res =0;
    for i in range(1,x+1):
        res+=i;
    return res

def int_sum_py2(x:int)->int:
    res =0;
    i = 1;
    while(i <= x):
        res+=i
        i+=1
    return res

def matrix_multi_py(x:list,y:list)->list:
    res = []
    for x1 in x:
        xarr = []
        for y1 in y:
            xarr.append(x1*y1);
        res.append(xarr)

    return res

def is_prime_py(x:int)->int:
    res=1;
    for i in range(2,int(x**0.5)+1):
        if x % i ==0:
            return i
    return res

def era_primes_py(n:int)->list:
    es = [2]
    Td = [True] * (n + 1)
    for i in range(3, n + 1, 2):
        if Td[i]:
            es.append(i)
            for j in range(i ** 2, n + 1, 2 * i):
                Td[j] = False
    return es

def twin_primes_py(val:int)->tuple:
    v = val+1;
    while (True):
        if is_prime_py(v)==1 and is_prime_py(v+2)==1:
            return (v,v+2)
        else:
            v +=1
    return (2,3)

def fibonacci_py(sev:int)->list:
    v = [1,1]
    for i in range(2,sev):
        v.append(v[i-2]+  v[i-1])
    return v

def groupby(csv)->dict:
    pd = pandas.read_csv(csv)
    dt = pd.groupby("DLMC")["TBMJ"].agg(["count","sum"]).to_dict()
    return dt

