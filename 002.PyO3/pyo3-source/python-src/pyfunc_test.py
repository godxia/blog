import pytest
import pyfunc

def test_two_multi_float_py():
    assert pyfunc.two_multi_float_py(3.0,4.0)==12.0

def test_two_divi_float_py():
    assert pyfunc.two_divi_float_py(12.0,4.0)==3.0

def test_int_sum_py():
    assert pyfunc.int_sum_py(10)==55

def test_int_sum_py2():
    assert pyfunc.int_sum_py2(10)==55

def test_matrix_multi_py():
    assert pyfunc.matrix_multi_py([1,2],[3,4])==[[3, 4], [6, 8]]

def test_is_prime_py_1():
    assert pyfunc.is_prime_py(5)==1

def test_is_prime_py_2():
    assert pyfunc.is_prime_py(10)==2

def test_twin_primes_py():
    assert pyfunc.twin_primes_py(5)==(11,13)

def test_fibonacci_py():
    assert pyfunc.fibonacci_py(5)==[1,1,2,3,5]

if __name__ == "__main__":
    pytest.main(["-s"])