use geo_types::{Polygon, LineString, Point, Geometry};
use std::convert::TryFrom;
use shapefile::Shape;

pub fn shapeToGeometry(shp_path:&str)-> Vec<Geometry>{
    let shps:Vec<Shape> = shapefile::read_shapes(shp_path)
    .expect(&format!("Could not open shapefile, error: {}", shp_path));
    let mut geometrys:Vec<Geometry> = Vec::new();
    for s in shps{
        geometrys.push(Geometry::<f64>::try_from(s).unwrap())
    }
    geometrys
}

pub struct shp;
pub trait ReadShapfile<T> {
    fn read_shp(shp_path:&str) -> T;
}


impl ReadShapfile<Vec<Polygon>> for shp{
    fn read_shp(shp_path:&str) -> Vec<Polygon>{
        let shp_read = shapefile::read_as::<_,
        shapefile::Polygon, shapefile::dbase::Record>(shp_path)
        .expect(&format!("Could not open polygon-shapefile, error: {}", shp_path));

    //构建输入参数
    let mut polygons:Vec<Polygon> = Vec::new();
    for (polygon, polygon_record) in shp_read {
        let geo_mpolygon: geo_types::MultiPolygon<f64> = polygon.into();
        for poly in geo_mpolygon.iter(){
            polygons.push(poly.to_owned());
        }
    }
    polygons
    }
}

impl ReadShapfile<Vec<LineString>> for shp{
    fn read_shp(shp_path:&str) -> Vec<LineString>{
        let shp_read = shapefile::read_as::<_,
            shapefile::Polyline, shapefile::dbase::Record>(shp_path)
        .expect(&format!("Could not open polyline-shapefile, error: {}", shp_path));

        //构建输入参数
        let mut linestrings:Vec<LineString> = Vec::new();
        for (pline, pline_record) in shp_read {
            let geo_mline: geo_types::MultiLineString<f64> = pline.into();
            for line in geo_mline.iter(){
                linestrings.push(line.to_owned());
            }
        }
        linestrings
    }
}

impl ReadShapfile<Vec<Point>> for shp{
    fn read_shp(shp_path:&str) -> Vec<Point>{
        let shp_read = shapefile::read_as::<_,
            shapefile::Point, shapefile::dbase::Record>(shp_path)
        .expect(&format!("Could not open polyline-shapefile, error: {}", shp_path));

        //构建输入参数
        let mut pnts:Vec<Point> = Vec::new();
        for (pnt, pnt_record) in shp_read {
            let geo_pnt: geo_types::Point<f64> = pnt.into();
            pnts.push(geo_pnt.to_owned());
        }
        pnts
    }
}


#[cfg(test)]
mod tests{
    use super::*;
    #[test]
    fn test_read_shp(){
        let shapefile: &str = "./data/shp/北京行政区划.shp";
        let poly:Vec<Polygon>= shp::read_shp(shapefile);
        println!("poly length = {:?}",poly.len());

        let shapefile: &str = "./data/shp/地铁.shp";
        let line:Vec<LineString>= shp::read_shp(shapefile);
        println!("line length = {:?}",line.len());

        let shapefile: &str = "./data/shp/大学.shp";
        let pnt:Vec<Point>= shp::read_shp(shapefile);
        println!("pnt length = {:?}",pnt.len());
    }
}