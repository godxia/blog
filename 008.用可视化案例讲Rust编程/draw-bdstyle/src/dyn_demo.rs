#![feature(trait_upcasting)]
use std::{any::{Any, TypeId}, ops::Add, borrow::Borrow};
#[derive(Debug)]
struct year{
    y:usize
}
#[derive(Debug,Clone)]
struct dog{
    name:String,
    age:usize,
}
fn double(s: &dyn Any){
    if let Some(v) = s.downcast_ref::<u32>() {
        println!("u32 double= {:?}",*v * 2);
    }
    else if let Some(v) = s.downcast_ref::<f32>() {
        println!("f32 double= {:?}",*v * 2.0);
    }
    else if let Some(v) = s.downcast_ref::<String>() {
        let x = v.clone();
        let x2 = v.clone();
        println!("string double= {:?}",x.add("_").add(&x2));
    }
    else if let Some(v) = s.downcast_ref::<year>() {
        let y = year{y:v.y +1};
        println!("year double= {:?}",y);
    }
    else if let Some(v) = s.downcast_ref::<dog>() {
        let mut d = dog{name:v.name.clone(), age:v.age};
        if d.age > 12{
            d.age =0;
        }
        else{
            d.age =d.age * 2;
        }
        println!("dog double= {:?}",d);
    }
}

trait my_type:std::fmt::Debug+'static+Any{
    fn double(&self);
}

impl my_type for i32{
    fn double(&self) {
        println!("i32 double= {:?}",self * self);
    }
}
impl my_type for f32{
    fn double(&self) {
        println!("f32 double= {:?}",self * self);
    }
}
impl my_type for String{
    fn double(&self) {
        println!("String double= {}_{}",self,self);
    }
}
impl my_type for dog{
    fn double(&self) {
        let mut d2 = self.clone();
        d2.age = d2.age +1;
        println!("dog double= {:?}",d2);
    }
}


fn show_my_type(s: impl my_type){
    if s.type_id() ==TypeId::of::<i32>(){
        println!("i32 = {:?}",s);
    }
    else if s.type_id() ==TypeId::of::<f32>(){
        println!("f32 = {:?}",s);
        
    }
    else if s.type_id() ==TypeId::of::<String>(){
        println!("String = {:?}",s);
    }
    else if s.type_id() ==TypeId::of::<dog>(){
        println!("dog = {:?}",s);
    }
    s.double();
}

// fn modify_if_u322(s: &mut dyn Any) {
//     if let Some(num) = s.downcast_mut::<u32>() {
//         *num = 42;
//     }
// }

#[cfg(test)]
mod tests{
    use super::*;
    #[test]
    fn test_double(){
        let i = 100;
        double(&i);

        let f = 3.2_f32;
        double(&f);

        let s = "hello world".to_string();
        double(&s);

        let y = year{y:1995};
        double(&y);

        let d = dog{name:"tom".to_string(),age:8};
        double(&d);

        let d2 = dog{name:"tom".to_string(),age:15};
        double(&d2);

    }

    #[test]
    fn test_my_type(){
        show_my_type(1);

        show_my_type(1.1);

        show_my_type("abcd".to_string());

        show_my_type(dog{name:"tom".to_string(),age:15});

        let f:f64 = 1.1;
        show_my_type(f);
    }
}