use std::{iter::zip, fmt::Debug, marker};
use plotly::{
    color::{NamedColor, Rgba},
    common::{Marker,Line,Mode},
    layout::{Layout, Center, DragMode, Mapbox, MapboxStyle, Margin},
    ScatterMapbox,ImageFormat,Plot
};

use geo_types::{Geometry,Point,LineString,Polygon,MultiPoint,MultiLineString,MultiPolygon};
use super::readShapefile;

#[derive(Debug,Clone)]
pub enum inputColor {
    NamedColor(NamedColor),
    Rgba(Rgba),
}

#[derive(Debug,Clone)]
pub struct traceParam<G>{
    pub geometrys:Vec<G>,
    pub colors: Vec<inputColor>,
    pub size: usize,
}

pub trait BuildTrace{
    fn build_trace(&self) -> Vec<Box<ScatterMapbox<f64,f64>>>;
}

impl BuildTrace for traceParam<Polygon>{
    fn build_trace(&self) -> Vec<Box<ScatterMapbox<f64,f64>>> {
        let mut traces:Vec<Box<ScatterMapbox<f64,f64>>> = Vec::new();
        for (poly,color) in zip(&self.geometrys,&self.colors) {
            let mut lat:Vec<f64>= Vec::new();
            let mut lon:Vec<f64>= Vec::new();
            for coord in poly.exterior(){
                lat.push(coord.y);
                lon.push(coord.x);
            }
            let mut trace: Box<ScatterMapbox<f64, f64>> = ScatterMapbox::new(lat, lon);
            trace = match color {
                inputColor::NamedColor(color) => {
                    MyTrace{color:*color,size:self.size,trace:trace}.set_trace(geo_type::Polygon)
                },
                inputColor::Rgba(color) => {
                    MyTrace{color:*color,size:self.size,trace:trace}.set_trace(geo_type::Polygon)
                },
                _ => panic!(""),
            };
            traces.push(trace);
            // for ipoly in poly.interiors(){
            //     let mut ilat:Vec<f64>= Vec::new();
            //     let mut ilon:Vec<f64>= Vec::new();
            //     for coord in poly.exterior(){
            //         ilat.push(coord.y);
            //         ilon.push(coord.x);
            //     }
            //     trace = ScatterMapbox::new(ilat, ilon);      
            //     trace = MyTrace{color:NamedColor::White,size:self.size,trace:trace}.set_trace(geo_type::Polygon);
            //     traces.push(trace);
            // }
        }
        traces
    }
}

impl BuildTrace for traceParam<LineString>{
    fn build_trace(&self) -> Vec<Box<ScatterMapbox<f64,f64>>> {
        let mut traces:Vec<Box<ScatterMapbox<f64,f64>>> = Vec::new();
        for (line,color) in zip(&self.geometrys,&self.colors) {
            let mut lat:Vec<f64>= Vec::new();
            let mut lon:Vec<f64>= Vec::new();
            for coord in line.coords(){
                lat.push(coord.y);
                lon.push(coord.x);
            }
            let mut trace: Box<ScatterMapbox<f64, f64>> = ScatterMapbox::new(lat, lon);
            trace = match color {
                inputColor::NamedColor(color) => {
                    MyTrace{color:*color,size:self.size,trace:trace}.set_trace(geo_type::Line)
                },
                inputColor::Rgba(color) => {
                    MyTrace{color:*color,size:self.size,trace:trace}.set_trace(geo_type::Line)
                },
                _ => panic!(""),
            };
            traces.push(trace);
        }
        traces
    }
}

impl BuildTrace for traceParam<Point>{
    fn build_trace(&self) -> Vec<Box<ScatterMapbox<f64,f64>>> {
        let mut traces:Vec<Box<ScatterMapbox<f64,f64>>> = Vec::new();
        for (pnt,color) in zip(&self.geometrys,&self.colors) {
            let mut trace: Box<ScatterMapbox<f64, f64>> = ScatterMapbox::new(vec![pnt.y()], vec![pnt.x()]);
            trace = match color {
                inputColor::NamedColor(color) => {
                    MyTrace{color:*color,size:self.size,trace:trace}.set_trace(geo_type::Point)
                },
                inputColor::Rgba(color) => {
                    MyTrace{color:*color,size:self.size,trace:trace}.set_trace(geo_type::Point)
                },
                _ => panic!(""),
            };
            traces.push(trace);
        }
        traces
    }
}

struct MyTrace<T>{
    color:T,
    size:usize,
    trace:Box<ScatterMapbox<f64,f64>>
}

enum geo_type{
    Point,
    Line,
    Polygon,
}
trait SetTrace<T> {
    fn set_trace(&self,geo_type:geo_type)->Box<ScatterMapbox<f64,f64>>;
}

impl SetTrace<NamedColor> for MyTrace<NamedColor>{
    fn set_trace(&self,geo_type:geo_type)->Box<ScatterMapbox<f64,f64>> {
        match geo_type{
            geo_type::Point =>{
                let t = *self.trace.to_owned()
                .marker(Marker::new().color(self.color)).show_legend(false);
                Box::new(t)
            },

            geo_type::Line =>{
                let t = *self.trace.to_owned()
                .line(Line::new().width(self.size as f64).color(self.color)).show_legend(false).mode(Mode::Lines);
                Box::new(t)
            },

            geo_type::Polygon=> {
                let t = *self.trace.to_owned()
                .fill(plotly::scatter_mapbox::Fill::ToSelf).fill_color(self.color).show_legend(false).mode(Mode::None);
                Box::new(t)
            },
            _ => panic!("")
        }
    }
}

impl SetTrace<Rgba> for MyTrace<Rgba>{
    fn set_trace(&self,geo_type:geo_type)->Box<ScatterMapbox<f64,f64>> {
        match geo_type{
            geo_type::Point =>{
                let t = *self.trace.to_owned()
                .marker(Marker::new().color(self.color)).show_legend(false);
                Box::new(t)
            },

            geo_type::Line =>{
                let t = *self.trace.to_owned()
                .line(Line::new().width(self.size as f64).color(self.color)).show_legend(false).mode(Mode::Lines);
                Box::new(t)
            },

            geo_type::Polygon=> {
                let t = *self.trace.to_owned()
                .fill(plotly::scatter_mapbox::Fill::ToSelf).fill_color(self.color).show_legend(false).mode(Mode::None);
                Box::new(t)
            },
            _ => panic!("")
        }
    }
}


pub fn plot_draw_trace(traces:Vec<Box<ScatterMapbox<f64,f64>>>,outimg: Option<&str>){
    let mut plot = Plot::new();
    for t in traces{
        plot.add_trace(t);
    }
    let layout = _get_layout(1024, 800, Center::new(39.9, 116.3),MapboxStyle::Dark);
    plot.set_layout(layout);
    match outimg {
        Some(out) => plot.write_image(out, ImageFormat::PNG, 1200, 900, 1.0),
        None => plot.show(),
    }
}

fn  _get_layout(width:usize, height:usize,cnt:Center,ms:MapboxStyle) -> Layout{
    Layout::new()
        .drag_mode(DragMode::Zoom)
        .margin(Margin::new().top(10).left(10).bottom(10).right(10))
        .width(width)
        .height(height)
        .mapbox(
            Mapbox::new()
                .style(ms)
                .access_token("pk.eyJ1IjoiYWxsZW5sdTIwMDgiLCJhIjoiY2xxZjNsaGtmMDd0ZTJqcWM1MzRmemx1NCJ9.TbiPQB6j1w9ilBP4pFHRRw")
                .center(cnt)
                .zoom(10),
        )
}

impl BuildTrace for traceParam<Geometry>{
    fn build_trace(&self) -> Vec<Box<ScatterMapbox<f64,f64>>> {
        let mut traces: Vec<Box<ScatterMapbox<f64,f64>>> = Vec::new();
        for (geom,color) in zip(self.geometrys.iter(),self.colors.iter()){
            let mut tr = match geom {
                Geometry::Point(_)=>{
                    let p:Point<_> = geom.to_owned().try_into().unwrap();
                    traceParam{geometrys:vec![p],colors:vec![color.to_owned()],size:self.size}.build_trace()
                },
                Geometry::MultiPoint(_)=>{
                    let p:MultiPoint<_> = geom.to_owned().try_into().unwrap();
                    let pnts:Vec<Point> = p.iter().map(|p|p.to_owned()).collect();
                    let color = (0..pnts.len()).map(|i|color.to_owned()).collect();
                    traceParam{geometrys:pnts,colors:color,size:self.size}.build_trace()
                },
               
                Geometry::LineString(_)=>{
                    let p:LineString<_> = geom.to_owned().try_into().unwrap();
                    traceParam{geometrys:vec![p],colors:vec![color.to_owned()],size:self.size}.build_trace()
                },
                Geometry::MultiLineString(_)=>{
                    let p:MultiLineString<_> = geom.to_owned().try_into().unwrap();
                    let lines:Vec<LineString> = p.iter().map(|p|p.to_owned()).collect();
                    let color = (0..lines.len()).map(|i|color.to_owned()).collect();
                    traceParam{geometrys:lines,colors:color,size:self.size}.build_trace()
                },
                
                Geometry::Polygon(_)=>{
                    let p:Polygon<_> = geom.to_owned().try_into().unwrap();
                    traceParam{geometrys:vec![p],colors:vec![color.to_owned()],size:self.size}.build_trace()
                },

                Geometry::MultiPolygon(_)=>{
                    let p:MultiPolygon<_> = geom.to_owned().try_into().unwrap();
                    let poly:Vec<Polygon> = p.iter().map(|p|p.to_owned()).collect();
                    let color = (0..poly.len()).map(|i|color.to_owned()).collect();
                    traceParam{geometrys:poly,colors:color,size:self.size}.build_trace()
                },
                _ => panic!("no geometry"),
            };
            traces.append(&mut tr);
        }
        traces
    }
}

mod tests{
    use crate::readShapefile::{self, ReadShapfile};

    use super::*;

    #[test]
    fn test_buildcolor(){
        let c1 = inputColor::NamedColor(NamedColor::Red);
        let c2 = inputColor::Rgba(Rgba::new(255,200,0,0.1));

        let poly1 = Polygon::new(
            LineString::from(vec![
                (116., 39.),(117., 39.), (117., 40.), (116., 40.),(116., 39.)]),
            vec![]
        );

        let poly2 = Polygon::new(
            LineString::from(vec![
                (116.5, 39.5),(117.3, 39.3), (117.5, 40.5), (116.5, 40.5),(116.5, 39.5)]),
            vec![]
        );

        let mut t1 = traceParam{geometrys:vec![poly1],colors:vec![c1],size:5}.build_trace();
        let mut t2 = traceParam{geometrys:vec![poly2],colors:vec![c2],size:5}.build_trace();
        t1.append(&mut t2);
        plot_draw_trace(t1,None);

    }

    #[test]
    fn draw_bd_style(){
        let shp1 = "./data/shp/北京行政区划.shp";
        let poly1:Vec<Polygon> = readShapefile::shp::read_shp(shp1);
        let colors:Vec<inputColor> = (0..poly1.len())
        .map(|x|inputColor::Rgba(Rgba::new(240,243,250,1.0))).collect();
        let mut t1 = traceParam{geometrys:poly1,colors:colors,size:0}.build_trace();
        
        let shp2 = "./data/shp/面状水系.shp";
        let poly2:Vec<Polygon> = readShapefile::shp::read_shp(&shp2);
        let colors:Vec<inputColor> = (0..poly2.len())
        .map(|x|inputColor::Rgba(Rgba::new(108,213,250,1.0))).collect();
        let mut t2 = traceParam{geometrys:poly2,colors:colors,size:0}.build_trace();

        let shp3 = "./data/shp/高速.shp";
        let line1:Vec<LineString> = readShapefile::shp::read_shp(&shp3);
        let colors:Vec<inputColor> = (0..line1.len())
        .map(|x|inputColor::Rgba(Rgba::new(255,182,118,1.0))).collect();
        let mut t3 = traceParam{geometrys:line1,colors:colors,size:2}.build_trace();
        
        t1.append(&mut t2);
        t1.append(&mut t3);
        plot_draw_trace(t1,None);
    }

    #[test]
    fn draw_db_style2(){
        let shp1 = "./data/shp/北京行政区划.shp";
        let color1 = inputColor::Rgba(Rgba::new(240,243,250,1.0));
        let shp2 = "./data/shp/面状水系.shp";
        let color2 = inputColor::Rgba(Rgba::new(108,213,250,1.0));
        let shp3 = "./data/shp/植被.shp";
        let color3 = inputColor::Rgba(Rgba::new(172,232,207,1.0));
        let shp4 = "./data/shp/高速.shp";
        let color4 = inputColor::Rgba(Rgba::new(255,182,118,1.0));
        let shp5 = "./data/shp/快速路.shp";
        let color5 = inputColor::Rgba(Rgba::new(255,216,107,1.0));

        let mut traces:Vec<Box<ScatterMapbox<f64,f64>>>= Vec::new();
        for (shp_path,color) in zip(vec![shp1,shp2,shp3,shp4,shp5]
            ,vec![color1,color2,color3,color4,color5]) {
            
            let gs = readShapefile::shapeToGeometry(shp_path);
            let colors:Vec<inputColor> = (0..gs.len())
                .map(|x|color.to_owned()).collect();
            let mut t = traceParam{geometrys:gs,colors:colors,size:2}.build_trace();
            traces.append(&mut t);
        }
        plot_draw_trace(traces,None);
    }


}