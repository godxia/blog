use std::iter::zip;
use geo_types::{Polygon, LineString};
use plotly::{
    color::Rgba,
    common::{Marker, Mode},
    layout::{Layout, Center, DragMode, Mapbox, MapboxStyle, Margin},
    ScatterMapbox,ImageFormat,Plot
};
use rand::Rng;

fn draw_shp(){
    let shp = shapefile::read_as::<_,
        shapefile::Polygon, shapefile::dbase::Record>(
            "./data/shp/北京行政区划.shp",
    ).expect(&format!("Could not open polygon-shapefile: './data/shp/北京行政区划.shp'"));

    let mut polygons:Vec<Polygon> = Vec::new();
    for (polygon, polygon_record) in shp {
        let geo_mpolygon: geo_types::MultiPolygon<f64> = polygon.into();
        for poly in geo_mpolygon.iter(){
            polygons.push(poly.to_owned());
        }
    }

    let mut trace_vec = Vec::new();
    for ps in polygons{
        let mut lon:Vec<f64> = Vec::new();
        let mut lat:Vec<f64> = Vec::new();
        for p in ps.exterior(){
            lon.push(p.x);
            lat.push(p.y);
        }
        let trace = ScatterMapbox::new(lat, lon).mode(Mode::None)
        .fill(plotly::scatter_mapbox::Fill::ToSelf)
        .fill_color(Rgba::new(0,0,255,0.5));
        trace_vec.push(trace);
    }

    let layout = Layout::new()
    .drag_mode(DragMode::Zoom)
    .margin(Margin::new().top(10).left(10).bottom(10).right(10))
    .width(1024)
    .height(700)
    .mapbox(
        Mapbox::new()
            .style(MapboxStyle::WhiteBg)
            .center(Center::new(39.9, 116.3))
            .zoom(9),
    );

    let mut plot = Plot::new();
    plot.set_layout(layout);
    for t in trace_vec.iter(){
        plot.add_trace(t.to_owned());
    }
    plot.show();

}


fn read_polygon(shapefile: &str) ->Vec<Polygon>{
    let shp = shapefile::read_as::<_,
        shapefile::Polygon, shapefile::dbase::Record>(
            shapefile,
    )
    .expect("Could not open polygon-shapefile");

    //构建输入参数
    let mut polygons:Vec<Polygon> = Vec::new();
    for (polygon, polygon_record) in shp {
        let geo_mpolygon: geo_types::MultiPolygon<f64> = polygon.into();
        for poly in geo_mpolygon.iter(){
            polygons.push(poly.to_owned());
        }
    }
    polygons
}

fn read_line(shapefile: &str) ->Vec<LineString>{
    let shp = shapefile::read_as::<_,
        shapefile::Polyline, shapefile::dbase::Record>(
            shapefile,
    )
    .expect(&format!("Could not open polyline-shapefile, error: {}", shapefile));

    //构建输入参数
    let mut linestrings:Vec<LineString> = Vec::new();
    for (pline, pline_record) in shp {
        let geo_mline: geo_types::MultiLineString<f64> = pline.into();
        for line in geo_mline.iter(){
            linestrings.push(line.to_owned());
        }
    }
    linestrings
}

fn build_polygon(polygons: &Vec<Polygon>,color:Rgba)->Vec<Box<ScatterMapbox<f64,f64>>>{
    let mut trace_vec = Vec::new();
    for ps in polygons{
        let mut lon:Vec<f64> = Vec::new();
        let mut lat:Vec<f64> = Vec::new();
        for p in ps.exterior(){
            lon.push(p.x);
            lat.push(p.y);
        }
        let trace = ScatterMapbox::new(lat, lon).mode(Mode::None)
        .fill(plotly::scatter_mapbox::Fill::ToSelf)
        .fill_color(color);
        trace_vec.push(trace);
    }
    trace_vec
}

fn build_line(lines: &Vec<LineString>,colors:Vec<Rgba>)->Vec<Box<ScatterMapbox<f64,f64>>>{
    let mut trace_vec = Vec::new();
    for (line,color) in zip(lines,colors){
        let mut lon:Vec<f64> = Vec::new();
        let mut lat:Vec<f64> = Vec::new();
        for p in line.coords(){
            lon.push(p.x);
            lat.push(p.y);
        }
        let trace = ScatterMapbox::new(lat, lon)
        .mode(Mode::Lines)
            .marker(Marker::new().color(color));
        trace_vec.push(trace);
    }
    trace_vec
}

fn draw_trace(traces:&Vec<Box<ScatterMapbox<f64,f64>>>){
    let mut plot = Plot::new();
    let layout = Layout::new()
    .drag_mode(DragMode::Zoom)
    .margin(Margin::new().top(10).left(10).bottom(10).right(10))
    .width(1024)
    .height(700)
    .mapbox(
        Mapbox::new()
            // .style(MapboxStyle::Dark)
            // .access_token("pk.eyJ1IjoiYWxsZW5sdTIwMDgiLCJhIjoiY2xxZjNsaGtmMDd0ZTJqcWM1MzRmemx1NCJ9.TbiPQB6j1w9ilBP4pFHRRw")
            .style(MapboxStyle::WhiteBg)
            .center(Center::new(39.9, 116.3))
            .zoom(9),
    );
    plot.set_layout(layout);
   
    for t in traces.iter(){
        plot.add_trace(t.to_owned());
    }
    plot.show();
}

fn draw_bd(){
    let poly1 = "./data/shp/北京行政区划.shp";
    let poly2 = "./data/shp/面状水系.shp";
    let poly3 = "./data/shp/植被.shp";
    let line1 = "./data/shp/地铁.shp";
    let line2 = "./data/shp/高速.shp";
    let line3 = "./data/shp/快速路.shp";

    let mut rng = rand::thread_rng();

    let t1 = build_polygon(
        &read_polygon(poly1), Rgba::new(240,243,250,1.0));
    let t2 = build_polygon(
        &read_polygon(poly2), Rgba::new(108,213,250,1.0));
    let t3 = build_polygon(
            &read_polygon(poly3), Rgba::new(172,232,207,1.0));
    
    let line1 = read_line(line1);
    let line1_color:Vec<Rgba> = (0..line1.len()).map(
        |x|Rgba::new(rng.gen_range(0..=255),rng.gen_range(0..=255),rng.gen_range(0..=255),0.7)
    ).collect();
    let t4: Vec<Box<ScatterMapbox<f64, f64>>> = build_line(&line1,line1_color);

    let line2 = read_line(line2);
    let line2_color:Vec<Rgba> = (0..line2.len()).map(|x|Rgba::new(255,182,118,1.0)).collect();
    let t5 = build_line(&line2,line2_color);

    let line3 = read_line(line3);
    let line3_color:Vec<Rgba> = (0..line3.len()).map(|x|Rgba::new(255,216,107,1.0)).collect();
    let t6 = build_line(&line3,line3_color);

    
    //plot.write_image("e:/abc.png", ImageFormat::PNG, 1920, 1200, 1.0);

}

#[cfg(test)]
mod tests{
    use super::*;

    #[test]
    fn draw(){
        draw_bd();
    }

    #[test]
    fn test_draw_shp(){
        draw_shp();
    }

    #[test]
    fn test_draw_2(){
        let poly1 = "./data/shp/北京行政区划.shp";
        let mut t1 = build_polygon(
            &read_polygon(poly1), Rgba::new(240,243,250,1.0));

        let poly2 = "./data/shp/面状水系.shp";
        let mut t2 = build_polygon(
            &read_polygon(poly2), Rgba::new(108,213,250,1.0));

        let line1 = "./data/shp/高速.shp";
        let line1 = read_line(line1);
        let line1_color:Vec<Rgba> = (0..line1.len()).map(|x|Rgba::new(255,182,118,1.0)).collect();
        let mut t3 = build_line(&line1,line1_color);
        
        t1.append(&mut t2);
        t1.append(&mut t3);
        draw_trace(&t1);
    }
}