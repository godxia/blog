# Rust的GIS开发教程系列
## Rust与Geos开发（7）
## 空间数据的表达与构建：特殊多边形Rect和Triangle

前面已经介绍了空间数据的几种数据类型，在Rust的geos中，还支持两种特殊的多边形，即：矩形`Rect`和 三角形`Triangle`。

很多人会奇怪，为什么要专门搞出这两种类型来，因为在很多时候，这两种图形会作为基本构型来用。

### 矩形：`Rect`

例如计算任何一个区域的extent（外接矩形），如果用`Polygon` 来构建矩形，最少需要五个点，而geos的`Rect`仅需要两个点就可以定义了：即左下和右上的坐标(min & max)；

```rust
pub struct Rect<T: CoordNum = f64> { /* private fields */ }
```

```rust
let rect = Rect::new(
    coord! { x: 0., y: 4.},
    coord! { x: 3., y: 10.},
);
```

小贴士：

> **Extent**（范围）是一个用于描述地图或特定图层在四个方向（北、南、东、西）上的边界坐标的类。它提供了地图或图层的空间范围信息，有助于确定地图上的可视化内容的位置和大小。
>
> Extent在GIS中具有多种作用，包括：
>
> * 定义地图范围：Extent可以用来确定地图的显示范围，即用户在屏幕上看到的地图区域。这有助于确保地图内容在正确的位置和大小上进行显示。
> 
> * 地理定位：Extent可以用于获取和设置地图的地理位置。例如，通过设置Extent的坐标，可以定位地图到特定的经纬度范围。
> 
> * 地图操作：Extent常常用于地图的各种操作，如漫游（滚动和缩放）、测量、裁剪等。这些操作需要知道地图或图层的边界坐标，以便正确地执行。
> 
> * 数据绑定：在GIS中，Extent还常用于将数据绑定到地图上。例如，将特定的地理数据集（如点、线或面）与Extent相关联，以便在地图上显示这些数据。
>

### 三角形：`Triangle`

与矩形是一样的，三角形也是经常作为基本构型来用，例如不规则三角网可以作为dem的构型，另外，三角形也是不规则多边形面积计算的主要方法：

![img](./img/007/001.jpg)


所以从语义上来说，`Triangle`一个有界的二维区域，其三个顶点由坐标定义。语义和有效性等价为多边形；此外，这三个顶点必须是相互不相等，且不能共线的存在。

结构很简单，三组坐标构成：
```rust
pub struct Triangle<T: CoordNum = f64>(pub Coord<T>, pub Coord<T>, pub Coord<T>);
```
> 注意，如果用polygon来构造三角形，得四个坐标，起始坐标和终点坐标必须重合，才能够构造出一个封闭多边形。
>

构造代码示例如下：

```rust
let triangle = Triangle::new(
    coord! { x: 0., y: 0. },
    coord! { x: 10., y: 20. },
    coord! { x: 20., y: -10. },
);

// 由三个坐标构成的三角形，与起讫坐标重合的多边形是相等的
assert_eq!(
    triangle.to_polygon(),
    polygon![
        (x: 0., y: 0.),
        (x: 10., y: 20.),
        (x: 20., y: -10.),
        (x: 0., y: 0.),
    ],
);
```

在geo算法里面，任意多边形，都可以获得相关的Rect和triangle，其中Rect是这多边形的extent，triangle是这个多边形的不规则三角网，如下所示：

我们来绘制一下北京市东城区的exetent和节点三角网：

![img](./img/007/003.png)

当然，绘制其他区的也是一样，比如海淀区的：

![img](./img/007/002.png)

代码如下：（不包括绘制代码）
```rust
use geo::prelude::*;
use geo::{MultiPolygon, TriangulateEarcut};
use shapefile::dbase::FieldValue;
use wkt::TryFromWkt;
use plotly::color::Rgba;
use rand::Rng;
use crate::plot;

fn func_rect_tri_demo(){
    let shp = shapefile::read_as::<_,
        shapefile::PolygonZ, shapefile::dbase::Record>(
        "../../data/beijing/beijing.shp",
    ).expect("Could not open polygon-shapefile");

    //构建输入参数,类似构建一个空的MultiPolygon
    let mut polygons:MultiPolygon<f64> = MultiPolygon::try_from_wkt_str("MULTIPOLYGON(((0 0)),((0 0)))").unwrap();
    
    //读取shapefile，并且取出指定的区域
    // 这里这个'a是生命周期限定符，里面的break 'a表示直接跳出'a代表的所有代码生命周期。这是Rust一个编码特性
    'a: for (polygon, polygon_record) in shp {
        for record in polygon_record{
            if record.0 == "NAME"{
                if match record.1{
                    FieldValue::Character(Some(s)) => s,
                    _=>String::from("")
                } == "海淀区"{
                    polygons = polygon.to_owned().into();
                    break 'a;
                }
            }
        }
    }
    //获取extent
    let rect = polygons.bounding_rect().unwrap();
    //获取节点三角网。
    let tris =  match polygons.iter().next() {  
        Some(first) => {first.earcut_triangles()},  
        None => panic!("Invalid")
    };

    //把三角形转化为MultiPolygon
    let tri_mply:Vec<MultiPolygon> = tris.iter().map(|tri| MultiPolygon::new(vec![tri.to_polygon()])).collect();
    let mut drawploy = vec![MultiPolygon::new(vec![rect.to_polygon()])];
    drawploy.extend(tri_mply);

    //根据vec中有多少个图形，来生成一个对应的随机颜色集合。
    let mut rng = rand::thread_rng();
    let rgba:Vec<Rgba> = (0..drawploy.len()).map(
        |i| Rgba::new(
            rng.gen_range(0..=255),
            rng.gen_range(0..=255),
            rng.gen_range(0..=255),0.5)
        ).collect();
    
    //绘图，代码略
    plot::draw_MultiPolygon(drawploy, rgba);
}

```