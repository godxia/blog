# Rust的GIS开发教程系列
## Rust与Geos开发（8）
## 空间数据的表达与构建：Geometry与总结

### Geometry类型

到今天，Rust GEOS的具体实现了几何实例数据结构类型就全部讲完了，最后一个是这些类型的总基类：Geometry，在其他的语言中，可以是一个基类，也可以是一个虚类（也叫抽象类），而在Rust中，它是一个枚举，因为从语言特性上来看，Rust的枚举也可以有各种不同的实现，还能起到泛型的作用，它的定义如下：

```rust
#[derive(Eq, PartialEq, Clone, Debug, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Geometry<T: CoordNum = f64> {
    Point(Point<T>),
    Line(Line<T>),
    LineString(LineString<T>),
    Polygon(Polygon<T>),
    MultiPoint(MultiPoint<T>),
    MultiLineString(MultiLineString<T>),
    MultiPolygon(MultiPolygon<T>),
    GeometryCollection(GeometryCollection<T>),
    Rect(Rect<T>),
    Triangle(Triangle<T>),
}
```

我们就可以如下写个功能函数：
```rust
pub fn draw_geometry(geometrys:Vec<geo::Geometry>,color:Vec<Rgba>){
    for geom in geometrys.iter() {
        match geom{
            Geometry::Point(_) => println!("point"),
            Geometry::Line(_) => println!("line"),
            Geometry::LineString(_) => {
                //处理LineString类型
                let line:LineString<_> = geom.to_owned().try_into().unwrap();
            }
            Geometry::Polygon(_)=> {
                //处理Polygon类型
            },
            Geometry::MultiPoint(_) => println!("MultiPoint"),
            Geometry::MultiLineString(_) => println!("MultiLineString"),
            Geometry::MultiPolygon(_) => println!("MultiPolygon"),
            _ => println!("none"),
        }
    }
}
```

调用的时候，仅需要把我们的几何类型包装成Geometry，然后直接扔进去就可以了：

```rust
fn test_draw_geometry(){
    let pt1 = Point::new(0.0, 0.0);
    let pt2 = Point::new(0.0, 0.0);
    let v:Vec<_> = vec![Geometry::try_from(pt1).unwrap(), Geometry::try_from(pt2).unwrap()];
    draw_geometry(v, vec![Rgba::new(255,255,0,1.0)]);

    let line:LineString<f64> = LineString::new(vec![coord!{x:0.0,y:0.0}, coord!{x:0.0,y:1.0}]);
    let v2:Vec<_> = vec![Geometry::try_from(line).unwrap()];
    draw_geometry(v2, vec![Rgba::new(255,255,0,1.0)])
}
```

Geometry可以匹配之后直接解析为任意几何类型，就可以进行具体的功能逻辑处理了，所以这种写法对于API调用的人来说，还是蛮省事了。

### 总结：
最后，来总结一下有关rust版geos的数据类型相关知识点：
1. Rust的GEOS库中没有所谓的点线面要素，只有一个超类`Geometry`，需要用`geo_types`（`geo`用的是`geo_types`中的类型）中的数据结构来定义点要素。
2. Coord是所有几何类型的基本组成部分，但是它本身不是一种几何类型。
3. 每种类型相互之间都可以进行转换，转换的原则：
   * 升维：多个对象集合之后进行转换：例如多个点的集合，可以转换成一条线，多条线可以转换成一个面。
   * 降维：高维对象拆分出来的成员对象，就是低维的对象：例如面是由外部环线和内部环线两种成员对象组成的。

4. 构造各类要素，有如下方法:
   1. 直接用坐标值进行构造。
   2. 用WKT字符串构造。
   3. 用GeoJSON构造之后转换成geo_types中的类型。

