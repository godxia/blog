# Rust的GIS开发教程系列
## Rust与Geos开发（3）
### 空间数据的表达与构建：线要素

上一节讲了点要素的构造方法，类推的话线要素也是一样的，将多个点有序的连接起来，就是一个线要素，所以线要素的结构如下所示：

```rust
pub struct LineString<T: CoordNum = f64>(pub Vec<Coord<T>>);
```

构造方法如下：

* 用`vec<coords>`集合构造
* 用`line_string <(x:,y:)>` 宏构造
* 用`vec<coords>迭代器` 构造
* 用 wkt 进行构造。

![img](./img/004/001.png)

当然有，有人会问，我已经有点要素了，可以直接转换成线么？或者我的线要素可以直接转换为点集么？
答案当然是可以，如下所示：

线和点之间是可以相互转换的：

* 点组成的集合，可以直接传入线构造函数里面：
  
  `let line1 = LineString::from(vec![p1, p2, p3]);`

* 线可以直接返回点的集合：

    `line1.points()`

如下所示：

![img](./img/004/002.png)

最后再来个简单的示例，用geojson + plotly来绘制北京的地铁线路图：

先看效果

![img](./img/004/004.png)

（这图怎么这么鸡里鸡气？好吧，本来就是Rust封装的javascript版的plotly，见第二节：番外：Rust对地理数据的简单可视化）

再说了：

![img](./img/000/00.jpg)

构造数据代码如下：

![img](./img/004/003.png)

绘图代码：

![img](./img/004/005.png)

![img](./img/004/006.png)

