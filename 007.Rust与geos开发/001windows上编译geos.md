# Rust的GIS开发教程系列
## Rust与Geos开发（1）
## 在windows上编译geos源码
### 前言
#### geos是什么？

Geos（Geometry Engine）是一个开源的空间集合引擎，用于处理和操作几何对象。它是一个用C++写成的通用库，GIS核心库之一。

Geos提供了许多几何对象和操作，例如点、线、多边形等，以及用于这些对象之间的空间关系计算和操作，如相交、接触、不相交、交叉、内部、包含、重叠等。它还提供了用于几何对象集合的操作，例如并集、距离、交集、对称差、凸包、缓冲区、简化、多边形组合等。

Geos支持多种数据格式的输入和输出，包括WKT（Well-Known Text）、WKB（Well-Known Binary）、geojson等。它还支持空间索引和查询操作，例如R-tree等，以提高对大量数据的查询性能。

Geos是一个核心依赖项，被许多其他软件和工具使用，例如PostGIS、QGIS、GDAL和Shapely等。

对比其他的几个开源库，如GDAL、JTS，GEOS的特点如下：

![img](./img/001/001.png)

注意：Geos是一个LGPL（GNU Lesser General Public LicenseGNU宽通用公共许可证）协议的库，与GPL的传染性开源不同，LGPL是允许打包独立程序的许可，也就是你可以把引用了LGPL库的程序进行独立打包，在外部引用Geos，但是不允许把这个库打包进去，否则就得开源。

Rust本身没有geos的实现，因为geos本身就是一个很成熟的GIS基础框架了，重复造轮子没啥意义，但是鉴于geos本身的API只有C/C++两种绑定，所以要用其他语言开调用开发geos，就需要进行跨语言绑定，例如Python的pygeos，就是用Python对geos api进行了Python语言的绑定，让Python程序员可以在不懂C/C++的情况下，直接用Python语法就可以调用geos的功能。

Rust也是一样，虽然Rust可以通过ffi调用c/c++，但是要写就得写Rust风格的代码，在同一个工程里面内置集中不同的代码风格对于写的人和读的人都是噩梦



所以自然就有了Rust的geos绑定：

https://github.com/georust/geos

这个库的主要贡献者，也是GDAL的主要开发者之一，著名GIS码农Corey Farwell……后面如果有时间我们还会介绍GDAL的Rust绑定—……恩，也是这个大神写的。

![img](./img/001/004.png)

如下所示，绑定之后，妥妥的Rust风格编码：

```Rust
use geos::Geom;

let gg1 = geos::Geometry::new_from_wkt("POLYGON ((0 0, 0 5, 6 6, 6 0, 0 0))").expect("invalid WKT");
let gg2 = geos::Geometry::new_from_wkt("POLYGON ((1 1, 1 3, 5 5, 5 1, 1 1))").expect("invalid WKT");
let mut gg3 = gg1.difference(&gg2).expect("difference failed");

gg3.normalize().expect("normalize failed");
assert_eq!(
    gg3.to_wkt_precision(0).expect("to_wkt failed"),
    "POLYGON ((0 0, 0 5, 6 6, 6 0, 0 0), (1 1, 5 1, 5 5, 1 3, 1 1))",
);
```

下面我们来看看如何在Rust中使用geos库。

首先Rust支撑动态编译和静态编译，二者区别的官方说法如下：
> * 静态编译是在编译时将程序的源代码和所有依赖的库文件编译成一个可执行文件。在执行程序时，操作系统加载这个可执行文件并将其放入内存中运行。由于所有的依赖都已经被编译进可执行文件中，所以在运行时不需要再去查找依赖的库文件。这样可以使得程序运行更快，但是可执行文件的大小会相对较大。
> 
> * 动态编译则是在程序运行时动态地加载所需要的库文件，而不是将它们静态地编译进可执行文件中。这样可以使得可执行文件的大小相对较小，但是在程序运行时需要查找并加载依赖的库文件，可能会造成一定的性能损失。

从软件项目来说，静态编译适合于需要快速启动并且对可执行文件大小没有要求的应用程序，例如操作系统内核和一些小型的命令行工具；而动态编译则适合于对可执行文件大小有要求，同时需要动态加载依赖库文件的应用程序，例如图形界面程序和网络应用程序。

下面看看如何在Rust里面对geos进行绑定：

### 编译geos

geos是用C++写成的，如果在linux上面可直接安装现成的库也是可以的用的，也可以用cmake来进行编译，但是在windows上面，cmake就比较麻烦了，所以一般有两种方式，下面分别来介绍一下


#### 在windows上利用vc来


这种方式可以用微软的vc工具包，直接对geos的源码进行编译：

我这里用的Microsoft Visual Studio 14.0：

![img](./img/001/011.png)

> 注意，这个需要安装windows sdk，根据不同的系统版本而定，一般来说得好几个G……
>
>![img](./img/001/014.png)
> 下载地址如下:
>
>  `https://developer.microsoft.com/en-us/windows/downloads/windows-sdk/`
-->

然后下载geos的源码，geos源码下载地址：

`https://libgeos.org/usage/download/`

![img](./img/001/007.png)

编译：

在windows上面，直接用解压软件先把源码解压一个能够找到文件夹里面，然后用命令行进入到解压的文件夹中：

> 我这里可能是VC（我用的是VC14）版本比较老，所以编译3.12.1 的时候会报错，我就选了一个比较低的版本：3.11
> 
> * 我这里vc14版本下面，最新的3.12.1报错（截止2023年12月11日）但是官方文档说在c++ 14下面是可以的，不知道啥情况……先不去纠结了。
> 

![img](./img/001/010.png)

然后开始编译：

首选用cmake进行预编译：
先进入到解压的文件夹里面，然后用命令生成makefiles
> 注意：不用忘记后面那个.. 这两个点表示编译上一级文件夹下面的内容
> 另外我机器上的VC是14版本，所以如果你安装的更高级别的版本，参考cmake -G的参数修改。

```bash
cmake -S . -B _build_vs2015x64 -G "Visual Studio 14 2015" -A x64 -DCMAKE_GENERATOR_TOOLSET=host=x64
```

![img](./img/001/012.png)


然后就可以进行编译了：

```bash
cmake --build _build_vs2015x64 --config Release -j 16 --verbose   
```

![img](./img/001/013.png)

这个编译的过程比较长，我这里花了好几分钟才编译完，里面可能会有一些警告，只要不报错，别管他就行：

![img](./img/001/015.png)

最后编译出来之后，在lib下面，就有我们需要的library库，而在bin下面，就有我们需要的dll动态链接库：

![img](./img/001/016.png)

![img](./img/001/017.png)

![img](./img/001/018.png)

注意lib包里面的内容是Rust 绑定需要的接口信息库，没有这个库就没法编译，编译完成之后，需要用dll库来执行。

然后把这个lib和bin，加入到windows的环境变量下面就行了，注意，lib需要下面两个环境变量：

* GEOS_LIB_DIR
* GEOS_VERSION

![img](./img/001/020.png)

而dll的路径要加入到path里面：

![img](./img/001/019.png)

然后测试代码如下：

```cargo
[dependencies]
geos = "8.3.0"
```

```Rust
extern crate geos;
use crate::geos::Geom;

fn main(){
    let gg1 = geos::Geometry::new_from_wkt("POLYGON ((0 0, 0 5, 6 6, 6 0, 0 0))").expect("invalid WKT");
    let gg2 = geos::Geometry::new_from_wkt("POLYGON ((1 1, 1 3, 5 5, 5 1, 1 1))").expect("invalid WKT");
    let gg3 = gg1.intersection(&gg2).expect("intersection failed");
    println!("gg3 area = {:?}",gg3.area().unwrap());
    println!("gg3 area = {:?}",gg3.to_wkt().unwrap());
    println!("gg3 area = {:?}",gg3.length().unwrap());
}
```

执行结果如下：
![img](./img/001/021.png)

#### 在linux上直接安装

> linux上面的编译与windows上类似，同样用cmake & make即可，略过

建议：还是在linux上面编译和开发c++的有关框架比较简单，如下所示：

1. 安装开发环境和geos库
```bash
sudo apt-get update
sudo apt-get install build-essential
sudo apt-get install libgeos-dev
```

![img](./img/001/023.png)

![img](./img/001/024.png)

2. 设定必要环境变量：

![img](./img/001/025.png)

3. 编译rust工程：

![img](./img/001/026.png)

然后可以阔以了……以了……了……

![img](./img/001/022.jpg)

（打完收工，待续未完）